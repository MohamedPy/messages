<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/message/{user}-{name}', 'Message\MessageController@create')
    ->name('user.messageForm');

Route::post('/message/{user}', 'Message\MessageController@store')
    ->name('send.user.message')
    ->middleware('throttle:5,1');

Route::get('/message/show/{id}', 'Message\MessageController@show')
    ->name('user.notification');
