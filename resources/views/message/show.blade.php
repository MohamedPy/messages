@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('Message From') {{ $notification->data['from']['name']}}</div>
                <div class="card-body">
                    {{ $notification->data['message'] }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
