@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('Message') {{$user->name}}</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $key => $error)
                            {{ $error }} @if($key < count($errors->all())) <br> @endif
                        @endforeach
                    </div>
                @endif

                <form action="{{route('send.user.message',[$user->id])}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">@lang('Message'):</label>
                        <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <button type="submit">@lang('Send Message')</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
