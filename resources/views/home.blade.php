@extends('layouts.app')
@push('styles')
    <style type="text/css">
     .admin{
         color:red;
         font-weight:600;
     }
    </style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if(count($notifications))
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang("Notifications")</div>

                    <div class="card-body">
                        @foreach($notifications as $notification)
                            <div>
                                New Message Form {{ $notification->data['from']['name']}}
                                <a href="{{route('user.notification',$notification->id)}}">
                                    Message Is {{ Str::limit( $notification->data['message'], 10) }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang("Users")</div>

                <div class="card-body">
                    @foreach($users as $user)
                            <div class="@if($user->isAdmin()) admin @endif">
                                @lang('Message') <a href="{{route('user.messageForm',[$user->id,$user->name])}}"> {{$user->name}}</a>
                            </div>
                    @endforeach
                </div>
            </div>
        </div>

        @include('message.components.alert')
    </div>
</div>
@endsection
