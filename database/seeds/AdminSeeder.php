<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $randomPassword = Str::random(10);
        $admin = factory(\App\User::class)->create([
            'is_admin' => \App\User::ADMIN,
            'password' => Hash::make($randomPassword),
        ]);
        echo("Email: $admin->email\nPassword: $randomPassword\n");
    }
}
