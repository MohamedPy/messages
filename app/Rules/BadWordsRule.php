<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Storage;

class BadWordsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $json = Storage::disk('local')->get('enBadWord.json');
        $json = json_decode($json, true);
        $arrayWords = explode(" ", $value);
        foreach ($arrayWords as $word){
            if (array_key_exists($word, $json)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('You used bad word and it not allow');
    }
}
