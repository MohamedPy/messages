<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        $notifications = $user->notifications()->get();
        $users = User::where('id','!=',$user->id)->get();
        return view('home',[
            "users" => $users,
            "notifications" => $notifications,
        ]);
    }
}
