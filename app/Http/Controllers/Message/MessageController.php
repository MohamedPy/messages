<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use App\Notifications\MessageUser;
use App\Notifications\SMSClient;
use App\Rules\BadWordsRule;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function show($id){
        $notification = auth()->user()->notifications()->where('id',$id)->firstOrFail();
        return view('message.show',[
            "notification" => $notification
        ]);
    }

    public function create(User $user){
        return view('message.create',[
            "user" => $user
        ]);
    }

    public function store(User $user,Request $request){

        $data = $request->validate(['message' => ['required', 'string', new BadWordsRule()]]);

        $user->notify(new MessageUser($data["message"],auth()->user()));

        return redirect()->route('home')->with('flash','The Message Sent Successfully');
    }
}
